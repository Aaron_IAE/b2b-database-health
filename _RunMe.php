<?php
$skip_setup = false;

$market_countries = [
	'AFI' => [
		'AO', 'BF', 'BI', 'BJ', 'BW', 'CD', 'CF', 'CG', 'CI', 'CM', 'CV', 'DJ', 'DZ', 'EG', 'EH', 'ER', 'ET', 'GA', 'GH', 'GM', 'GN', 'GQ', 'GW', 'KE', 'KM', 'LR', 'LS', 'LY', 'MA', 'MG', 'ML', 'MR', 'MU', 'MW', 'MZ', 'NA', 'NE', 'NG', 'RW', 'SC', 'SD', 'SH', 'SL', 'SN', 'SO', 'SS', 'ST', 'SZ', 'TD', 'TG', 'TN', 'TZ', 'UG', 'ZA', 'ZM', 'ZW'
	],
	'APA' => [
		'AS', 'AU', 'BN', 'CC', 'CK', 'CX', 'FJ', 'FM', 'GS', 'GU', 'HM', 'ID', 'IO', 'KH', 'KI', 'KP', 'KR', 'LA', 'MH', 'MM', 'MP', 'MV', 'MY', 'NF', 'NR', 'NU', 'NZ', 'PG', 'PH', 'PK', 'PN', 'PW', 'SB', 'SG', 'TH', 'TK', 'TL', 'TO', 'TV', 'UM', 'VN', 'VU', 'WS'
	],
	'BNL' => [
		'BE', 'LU', 'NL'
	],
	'CEE' => [
		'AL', 'BA', 'BG', 'CZ', 'EE', 'HR', 'HU', 'LT', 'LV', 'MD', 'ME', 'MK', 'PL', 'RO', 'RS', 'SI', 'SK', 'UA'
	],
	'DAC' => [
		'AT', 'CH', 'DE', 'LI'
	],
	'FRA' => [
		'BL', 'FR', 'GF', 'GP', 'MC', 'MF', 'MQ', 'NC', 'PF', 'PM', 'RE', 'TF', 'VC', 'WF', 'YT'
	],
	'GRC' => [
		'CN', 'HK', 'MO', 'TW'
	],
	'IBE' => [
		'AD', 'ES', 'GI', 'PT'
	],
	'IIG' => [
		'CY', 'GR', 'IL', 'IT', 'MT', 'SM', 'VA'
	],
	'ISC' => [
		'BD', 'BT', 'IN', 'LK', 'NP'
	],
	'JPN' => [
		'JP'
	],
	'LAT' => [
		'AG', 'AI', 'AQ', 'AR', 'AW', 'BB', 'BM', 'BO', 'BQ', 'BR', 'BS', 'BZ', 'CL', 'CO', 'CR', 'CU', 'CW', 'DM', 'DO', 'EC', 'FK', 'GD', 'GT', 'GY', 'HN', 'HT', 'JM', 'KN', 'KY', 'LC', 'MS', 'MX', 'NI', 'PA', 'PE', 'PR', 'PY', 'SR', 'SV', 'SX', 'TC', 'TT', 'UY', 'VE', 'VG', 'VI'
	],
	'MET' => [
		'AE', 'AF', 'BH', 'IQ', 'IR', 'JO', 'KW', 'LB', 'OM', 'PS', 'QA', 'SA', 'SY', 'TR', 'YE'
	],
	'NAM' => [
		'CA', 'US'
	],
	'NOR' => [
		'AX', 'BV', 'DK', 'FI', 'FO', 'GL', 'IS', 'NO', 'SE', 'SJ'
	],
	'RCA' => [
		'AM', 'AZ', 'BY', 'GE', 'KG', 'KZ', 'MN', 'RU', 'TJ', 'TM', 'UZ'
	],
	'UKI' => [
		'GB', 'GG', 'IE', 'IM', 'JE'
	],
	'N/A' => [
		// blank for a reason
	],
	'ALL' => [
		'AO', 'BF', 'BI', 'BJ', 'BW', 'CD', 'CF', 'CG', 'CI', 'CM', 'CV', 'DJ', 'DZ', 'EG', 'EH', 'ER', 'ET', 'GA', 'GH', 'GM', 'GN', 'GQ', 'GW', 'KE', 'KM', 'LR', 'LS', 'LY', 'MA', 'MG', 'ML', 'MR', 'MU', 'MW', 'MZ', 'NA', 'NE', 'NG', 'RW', 'SC', 'SD', 'SH', 'SL', 'SN', 'SO', 'SS', 'ST', 'SZ', 'TD', 'TG', 'TN', 'TZ', 'UG', 'ZA', 'ZM', 'ZW',
		'AS', 'AU', 'BN', 'CC', 'CK', 'CX', 'FJ', 'FM', 'GS', 'GU', 'HM', 'ID', 'IO', 'KH', 'KI', 'KP', 'KR', 'LA', 'MH', 'MM', 'MP', 'MV', 'MY', 'NF', 'NR', 'NU', 'NZ', 'PG', 'PH', 'PK', 'PN', 'PW', 'SB', 'SG', 'TH', 'TK', 'TL', 'TO', 'TV', 'UM', 'VN', 'VU', 'WS',
		'BE', 'LU', 'NL',
		'AL', 'BA', 'BG', 'CZ', 'EE', 'HR', 'HU', 'LT', 'LV', 'MD', 'ME', 'MK', 'PL', 'RO', 'RS', 'SI', 'SK', 'UA',
		'AT', 'CH', 'DE', 'LI',
		'BL', 'FR', 'GF', 'GP', 'MC', 'MF', 'MQ', 'NC', 'PF', 'PM', 'RE', 'TF', 'VC', 'WF', 'YT',
		'CN', 'HK', 'MO', 'TW',
		'AD', 'ES', 'GI', 'PT',
		'CY', 'GR', 'IL', 'IT', 'MT', 'SM', 'VA',
		'BD', 'BT', 'IN', 'LK', 'NP',
		'JP',
		'AG', 'AI', 'AQ', 'AR', 'AW', 'BB', 'BM', 'BO', 'BQ', 'BR', 'BS', 'BZ', 'CL', 'CO', 'CR', 'CU', 'CW', 'DM', 'DO', 'EC', 'FK', 'GD', 'GT', 'GY', 'HN', 'HT', 'JM', 'KN', 'KY', 'LC', 'MS', 'MX', 'NI', 'PA', 'PE', 'PR', 'PY', 'SR', 'SV', 'SX', 'TC', 'TT', 'UY', 'VE', 'VG', 'VI',
		'AE', 'AF', 'BH', 'IQ', 'IR', 'JO', 'KW', 'LB', 'OM', 'PS', 'QA', 'SA', 'SY', 'TR', 'YE',
		'CA', 'US',
		'AX', 'BV', 'DK', 'FI', 'FO', 'GL', 'IS', 'NO', 'SE', 'SJ',
		'AM', 'AZ', 'BY', 'GE', 'KG', 'KZ', 'MN', 'RU', 'TJ', 'TM', 'UZ',
		'GB', 'GG', 'IE', 'IM', 'JE'
	]
];

/**
 *
 * Print an item in the format "$item started at HH:MM ... " or "$item started at HH:MM."
 *
 * @param string $item The text to print
 * @param bool $includeEnd whether to include a newline (false) or " ... " (true). Short for "this will have logEnd() called after the log is done"
 *
 * @return void
 */
function logStart(String $item, Bool $includeEnd = false): void {
	fwrite(STDOUT, str_repeat(' ', 100) . "\r" );
	$now = new DateTime();
	$out = $item . ' started at ' . $now->format('H:i');
	if ($includeEnd) {
		$out .= ' ... ';
	} else {
		$out .= '.' . "\n";
	}
	fwrite(STDOUT, $out);
}

/**
 *
 * Print a message in the format "done at HH:MM." followed by a newline. Designed for use after logStart($item,true).
 *
 * @return void
 */
function logEnd(): void {
	$now = new DateTime();
	fwrite(STDOUT, "done at " . $now->format('H:i') . '. ' . "\n");
}

/**
 *
 * Print a message in the format "$note HH:MM." followed by a newline. Designed for use after logStart($item,true).
 *
 * @param string $note The message to output.
 *
 * @return void
 */
function logMisc(String $note): void {
	$now = new DateTime();
	$out = $note . $now->format('H:i') . '.';
	fwrite(STDOUT, $out . "\n" );
}

/**
 *
 * Clear the current STDOUT line (up to 100 characters) and print a message in
 * the format "$note HH:MM." then returns the cursor to the start of the line.
 * Designed for use when you need a progress indicator, but don't need it to
 * persist.
 *
 * @param string $note The message to output.
 *
 * @return void
 */
function logOneLine(String $note): void {
	$now = new DateTime();
	fwrite(STDOUT, str_repeat(' ', 100) . "\r" );
	$out = $note . $now->format('H:i') . '.' . "\r";
	fwrite(STDOUT, $out);
}

/**
 *
 * Import a CSV file to a pre-defined PDO connection to a SQLite database.
 *
 * @param PDO $pdo The database connection
 * @param string $csv_path The full path to the CSV file to import
 * @param array $options Associative Array of options to use:
 * 		'delimiter'	: The CSV field delimiter. Defaults to ','
 * 		'table'		: The name of the table to use. Defaults to the basename of
 * 						the file (no path, no extension) with non-alphanumeric
 * 						characters stripped.
 * 		'fields'	: 1-D array of field names to import data into. Defaults
 * 						to the data in the first line of the file, with non-
 * 						alphanumeric characters stripped, and lower-cased.
 *
 * @return array
 */
function import_csv_to_sqlite(&$pdo, $csv_path, $options = array()): array {
	$now = new DateTime();
	extract($options);

	if (($csv_handle = fopen($csv_path, "r")) === FALSE)
		throw new Exception('Cannot open CSV file');

	if(! isset($delimiter)) {
		$delimiter = ',';
	}

	if(! isset($table)) {
		$table = preg_replace("/[^A-Z0-9]/i", '', basename($csv_path));
	}

	if(! isset($fields)) {
		$fields = array_map(function ($field){
			return strtolower(preg_replace("/[^A-Z0-9]/i", '', $field));
		}, fgetcsv($csv_handle, 0, $delimiter));
	}

	$create_fields_str = join(', ', array_map(function ($field){
		return "$field TEXT NULL";
	}, $fields));

	$pdo->beginTransaction();

	$create_table_sql = "CREATE TABLE IF NOT EXISTS $table ($create_fields_str)";
	logStart('SQL table creation');
	$pdo->exec($create_table_sql);

	$insert_fields_str = join(', ', $fields);
	$insert_values_str = join(', ', array_fill(0, count($fields),  '?'));
	$insert_sql = "INSERT INTO $table ($insert_fields_str) VALUES ($insert_values_str)";
	$insert_sth = $pdo->prepare($insert_sql);

	$inserted_rows = 0;
	while (($data = fgetcsv($csv_handle, 0, $delimiter)) !== FALSE) {
		$insert_sth->execute($data);
		$inserted_rows++;
		if ($inserted_rows % 10000 == 0) {
			fwrite(STDOUT,"\t" . 'Inserted ' . number_format($inserted_rows) . ' rows' . "\r");
		}
	}
	fwrite(STDOUT,"\t" . 'Inserted ' . number_format($inserted_rows) . ' rows' . "\n");

	$pdo->commit();

	fclose($csv_handle);
	unset($data);

	return array(
		'table' => $table,
		'fields' => $fields,
		'insert' => $insert_sth,
		'inserted_rows' => $inserted_rows
	);

}

/* use this to bypass the import */
if (file_exists('mydb.sq3')) {
	$skip_setup = true;
}

$db = new PDO('sqlite:mydb.sq3');
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$options['table'] = 'B2BCFC';

if ($skip_setup) { // Import Bypassed
	$res = $db->query("PRAGMA table_info(`" . $options['table'] . "`);");
	$head = $res->fetchAll(PDO::FETCH_ASSOC);
	foreach ($head as $row => $results) {
		$headers[] = $results['name'];
	}
	logMisc('Skipping import at ');
} else { // Import Not Bypassed
	$ref = import_csv_to_sqlite($db, 'ContactFieldCompleteness.csv', $options);
	$headers = $ref['fields'];
}

logStart('Program');

if (file_exists('output.csv')) {
	logMisc('Skipping top half of sheet at ');
} else {
	foreach ($market_countries as $market => $countries) {
		logStart($market);
		if ($market !== 'N/A' && $market !== 'ALL') {
			$filled_count = "SELECT COUNT(*) AS `#Filled` FROM `B2BCFC` WHERE (length(HEAD) > 0 AND `country` in (COUNTRYCODE))";
			$empty_count = "SELECT COUNT(*) AS `#Empty` FROM `B2BCFC` WHERE (((length(HEAD) = 0) OR typeof(HEAD) = 'null') AND `country` in (COUNTRYCODE))";
			$full = "SELECT ($filled_count) AS `#Filled`, ($empty_count) AS `#Empty`;";
			$qry = preg_replace("/COUNTRYCODE/", "'" . implode("' ,'", $countries) . "'", $full);
		} elseif($market === 'N/A') { // 'N/A' Market
			$filled_count = "SELECT COUNT(*) FROM `B2BCFC` WHERE (length(HEAD) > 0) AND (`country` NOT IN (COUNTRYCODE))";
			$empty_count = "SELECT COUNT(*) FROM `B2BCFC` WHERE ((length(HEAD) = 0) OR typeof(HEAD) = 'null') AND (`country` NOT IN (COUNTRYCODE))";
			$full = "SELECT ($filled_count) AS `#Filled`, ($empty_count) AS `#Empty`;";
			$qry = preg_replace("/COUNTRYCODE/", "'" . implode("' ,'", $market_countries['ALL']) . "'", $full);
		} else { // 'ALL' Market
			$filled_count = "SELECT COUNT(*) FROM `B2BCFC` WHERE (length(HEAD) > 0)";
			$empty_count = "SELECT COUNT(*) FROM `B2BCFC` WHERE ((length(HEAD) = 0) OR typeof(HEAD) = 'null')";
			$full = "SELECT ($filled_count) AS `#Filled`, ($empty_count) AS `#Empty`;";
			$qry = $full;
		}
		$countOfHeaders = count($headers);
		$currentHeaderCount = 1;

		foreach ($headers as $head) {
			logOneLine("\t\t" . '[' . $currentHeaderCount . '/' . $countOfHeaders . '] ' . $head . ' at ');
			$sql = preg_replace("/HEAD/", "`" . $head . "`", $qry); // Not using a prepared statement because SQLite is caching the results
			$res = $db->query($sql);
			if ( ! $res) {
				var_export($db->errorInfo());die();
			}
			$result = $res->fetch(PDO::FETCH_ASSOC);
			$out[$head][$market] = $result;

			$file1[$head][$market]['%Filled'] = (int)$result['#Filled'] / ((int)$result['#Filled'] + (int)$result['#Empty']);
			$file1[$head][$market]['%Empty'] = (int)$result['#Empty'] / ((int)$result['#Filled'] + (int)$result['#Empty']);
			$file1[$head][$market]['#Filled'] = (int)$result['#Filled'];
			$file1[$head][$market]['#Empty'] = (int)$result['#Empty'];
			$currentHeaderCount++;

			unset($res); // Memory clear and cache-buster
			unset($result);
		}
		logOneLine("done");
	}
	unset($market);

	logStart('Creating file \'output.csv\'');
	$handle = fopen('output.csv', 'w');

	logStart("\tWriting headers",true);
	$head1 = explode(',', 'Field,' . str_repeat('%Filled,%Empty,#Filled,#Empty,', count($market_countries)));
	fputcsv($handle, $head1);

	$head2A = 'Field,';
	foreach ($market_countries as $m_code => $value) {
		$head2A .= str_repeat($m_code . ',', 4);
	}
	$head2 = explode(',', $head2A);

	fputcsv($handle, $head2);
	logEnd();

	foreach ($file1 as $header => $market) {
		$line[] = $header;
		foreach ($market as $mName => $values) {
			$line[] = $values['%Filled'];
			$line[] = $values['%Empty'];
			$line[] = $values['#Filled'];
			$line[] = $values['#Empty'];
		}
		fputcsv($handle, $line);
		unset($line);
	}

	fclose($handle);
	logEnd();

	if (isset($data)) {
		unset($data);
	}
}


if (file_exists('results.dat')) {
	logMisc('Phase 1 ended. Skipping Phase 2 (.dat exists)');
} else {
	logMisc('Phase 1 ended. Starting Phase 2 at ');

	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/

	$sqlWithOther = <<<SQL
		SELECT `FIELD`, Count(*) AS `#Filled`
		FROM `B2BCFC`
		WHERE `FIELD` in (VALIDS)
		COUNTRY
		GROUP BY `FIELD`
		UNION
		SELECT 'Other' AS `FIELD`, Count(*) AS `#Filled`
		FROM `B2BCFC`
		WHERE (length(`FIELD`) > 0 AND `FIELD` NOT IN (VALIDS))
		COUNTRY
		UNION
		SELECT '(Blank)' AS `FIELD`, Count(*) AS `#Filled`
		FROM `B2BCFC`
		WHERE length(`FIELD`) = 0
		COUNTRY
		ORDER BY `#Filled` DESC;
SQL;

	$sqlWithoutOther = <<<SQL
		SELECT `FIELD`, Count(*) AS `#Filled`
		FROM `B2BCFC`
		WHERE length(`FIELD`) > 0
		COUNTRY
		GROUP BY `FIELD`
		UNION
		SELECT '(Blank)' AS `FIELD`, Count(*) AS `#Filled`
		FROM `B2BCFC`
		WHERE length(`FIELD`) = 0
		COUNTRY
		ORDER BY `#Filled` DESC;
SQL;

	$field_list = [
		'salutation' => [
			'include_other' => true,
			'valids' => "'Mr.', 'Dr.', 'Mrs.', 'Ms.'"
		],
		'function' => [
			'include_other' => false,
			'valids' => "all"
		],
		'departmentcategory' => [
			'include_other' => false,
			'valids' => "all"
		],
		'jobrole' => [
			'include_other' => false,
			'valids' => "all"
		],
		'department' => [
			'include_other' => false,
			'valids' => "all"
		],
		'language' => [
			'include_other' => true,
			'valids' => "'aar', 'aav', 'abk', 'ace', 'ach', 'ada', 'ady', 'afa', 'afh', 'afr', 'ain', 'aka', 'akk', 'alb', 'ale', 'alg', 'alt', 'alv', 'amh', 'ang', 'anp', 'apa', 'aqa', 'aql', 'ara', 'arc', 'arg', 'arm', 'arn', 'arp', 'art', 'arw', 'asm', 'ast', 'ath', 'auf', 'aus', 'ava', 'ave', 'awa', 'awd', 'aym', 'azc', 'aze', 'bad', 'bai', 'bak', 'bal', 'bam', 'ban', 'baq', 'bas', 'bat', 'bej', 'bel', 'bem', 'ben', 'ber', 'bho', 'bih', 'bik', 'bin', 'bis', 'bla', 'bnt', 'bod', 'bos', 'bra', 'bre', 'btk', 'bua', 'bug', 'bul', 'bur', 'byn', 'cad', 'cai', 'car', 'cat', 'cau', 'cba', 'ccn', 'ccs', 'cdc', 'cdd', 'ceb', 'cel', 'ces', 'cha', 'chb', 'che', 'chg', 'chi', 'chk', 'chm', 'chn', 'cho', 'chp', 'chr', 'chs', 'chu', 'chv', 'chy', 'cmc', 'cop', 'cor', 'cos', 'cpe', 'cpf', 'cpp', 'cre', 'crh', 'crp', 'csb', 'csu', 'cus', 'cym', 'cze', 'dak', 'dan', 'dar', 'day', 'del', 'den', 'deu', 'dgr', 'din', 'div', 'dmn', 'doi', 'dra', 'dsb', 'dua', 'dum', 'dut', 'dyu', 'dzo', 'efi', 'egx', 'egy', 'eka', 'ell', 'elx', 'eng', 'enm', 'epo', 'est', 'esx', 'euq', 'eus', 'ewe', 'ewo', 'fan', 'fao', 'fas', 'fat', 'fij', 'fil', 'fin', 'fiu', 'fon', 'fox', 'fra', 'fre', 'frm', 'fro', 'frr', 'frs', 'fry', 'ful', 'fur', 'gaa', 'gay', 'gba', 'gem', 'geo', 'ger', 'gez', 'gil', 'gla', 'gle', 'glg', 'glv', 'gme', 'gmh', 'gmq', 'gmw', 'goh', 'gon', 'gor', 'got', 'grb', 'grc', 'gre', 'grk', 'grn', 'gsw', 'guj', 'gwi', 'hai', 'hat', 'hau', 'haw', 'heb', 'her', 'hil', 'him', 'hin', 'hit', 'hmn', 'hmo', 'hmx', 'hok', 'hrv', 'hsb', 'hun', 'hup', 'hye', 'hyx', 'iba', 'ibo', 'ice', 'ido', 'iii', 'iir', 'ijo', 'iku', 'ile', 'ilo', 'ina', 'inc', 'ind', 'ine', 'inh', 'ipk', 'ira', 'iro', 'isl', 'ita', 'itc', 'jav', 'jbo', 'jpn', 'jpr', 'jpx', 'jrb', 'kaa', 'kab', 'kac', 'kal', 'kam', 'kan', 'kar', 'kas', 'kat', 'kau', 'kaw', 'kaz', 'kbd', 'kdo', 'kha', 'khi', 'khm', 'kho', 'kik', 'kin', 'kir', 'kmb', 'kok', 'kom', 'kon', 'kor', 'kos', 'kpe', 'krc', 'krl', 'kro', 'kru', 'kua', 'kum', 'kur', 'kut', 'lad', 'lah', 'lam', 'lao', 'lat', 'lav', 'lez', 'lim', 'lin', 'lit', 'lol', 'loz', 'ltz', 'lua', 'lub', 'lug', 'lui', 'lun', 'luo', 'lus', 'mac', 'mad', 'mag', 'mah', 'mai', 'mak', 'mal', 'man', 'mao', 'map', 'mar', 'mas', 'may', 'mdf', 'mdr', 'men', 'mga', 'mic', 'min', 'mis', 'mkd', 'mkh', 'mlg', 'mlt', 'mnc', 'mni', 'mno', 'moh', 'mon', 'mos', 'mri', 'msa', 'mul', 'mun', 'mus', 'mwl', 'mwr', 'mya', 'myn', 'myv', 'nah', 'nai', 'nap', 'nau', 'nav', 'nbl', 'nde', 'ndo', 'nds', 'nep', 'new', 'ngf', 'nia', 'nic', 'niu', 'nld', 'nno', 'nob', 'nog', 'non', 'nor', 'nqo', 'nso', 'nub', 'nwc', 'nya', 'nym', 'nyn', 'nyo', 'nzi', 'oci', 'oji', 'omq', 'omv', 'ori', 'orm', 'osa', 'oss', 'ota', 'oto', 'paa', 'pag', 'pal', 'pam', 'pan', 'pap', 'pau', 'peo', 'per', 'phi', 'phn', 'plf', 'pli', 'pol', 'pon', 'por', 'poz', 'pqe', 'pqw', 'pra', 'pro', 'pus', 'qaa-qtz', 'que', 'qwe', 'raj', 'rap', 'rar', 'rcf', 'roa', 'roh', 'rom', 'ron', 'rum', 'run', 'rup', 'rus', 'sad', 'sag', 'sah', 'sai', 'sal', 'sam', 'san', 'sas', 'sat', 'scc', 'scn', 'sco', 'scr', 'sdv', 'sel', 'sem', 'sga', 'sgn', 'shn', 'sid', 'sin', 'sio', 'sit', 'sla', 'slk', 'slo', 'slv', 'sma', 'sme', 'smi', 'smj', 'smn', 'smo', 'sms', 'sna', 'snd', 'snk', 'sog', 'som', 'son', 'sot', 'spa', 'sqi', 'srd', 'srn', 'srp', 'srr', 'ssa', 'ssw', 'suk', 'sun', 'sus', 'sux', 'swa', 'swe', 'syc', 'syd', 'syr', 'tah', 'tai', 'tam', 'tat', 'tbq', 'tel', 'tem', 'ter', 'tet', 'tgk', 'tgl', 'tha', 'tib', 'tig', 'tir', 'tiv', 'tkl', 'tlh', 'tli', 'tmh', 'tog', 'ton', 'tpi', 'trk', 'tsi', 'tsn', 'tso', 'tuk', 'tum', 'tup', 'tur', 'tut', 'tuw', 'tvl', 'twi', 'tyv', 'udm', 'uga', 'uig', 'ukr', 'umb', 'und', 'urd', 'urj', 'uzb', 'vai', 'ven', 'vie', 'vol', 'vot', 'wak', 'wal', 'war', 'was', 'wel', 'wen', 'wln', 'wol', 'xal', 'xgn', 'xho', 'xnd', 'yao', 'yap', 'yid', 'yor', 'ypk', 'zap', 'zbl', 'zen', 'zgh', 'zha', 'zho', 'zhx', 'zle', 'zls', 'zlw', 'znd', 'zul', 'zun', 'zxx', 'zza'",
			'Full' => [
				'aar' => 'Afar',
				'aav' => 'Austro-Asiatic languages',
				'abk' => 'Abkhazian',
				'ace' => 'Achinese',
				'ach' => 'Acoli',
				'ada' => 'Adangme',
				'ady' => 'Adyghe',
				'afa' => 'Afro-Asiatic languages',
				'afh' => 'Afrihili',
				'afr' => 'Afrikaans',
				'ain' => 'Ainu (Japan)',
				'aka' => 'Akan',
				'akk' => 'Akkadian',
				'alb' => 'Albanian',
				'ale' => 'Aleut',
				'alg' => 'Algonquian languages',
				'alt' => 'Southern Altai',
				'alv' => 'Atlantic-Congo languages',
				'amh' => 'Amharic',
				'ang' => 'Old English (c. 450–1100)',
				'anp' => 'Angika',
				'apa' => 'Apache languages',
				'aqa' => 'Alacalufan languages',
				'aql' => 'Algic languages',
				'ara' => 'Arabic',
				'arc' => 'Official Aramaic (700–300 BCE)',
				'arg' => 'Aragonese',
				'arm' => 'Armenian',
				'arn' => 'Mapudungun',
				'arp' => 'Arapaho',
				'art' => 'Artificial languages',
				'arw' => 'Arawak',
				'asm' => 'Assamese',
				'ast' => 'Asturian',
				'ath' => 'Athapascan languages',
				'auf' => 'Arauan languages',
				'aus' => 'Australian languages',
				'ava' => 'Avaric',
				'ave' => 'Avestan',
				'awa' => 'Awadhi',
				'awd' => 'Arawakan languages',
				'aym' => 'Aymara',
				'azc' => 'Uto-Aztecan languages',
				'aze' => 'Azerbaijani',
				'bad' => 'Banda languages',
				'bai' => 'Bamileke languages',
				'bak' => 'Bashkir',
				'bal' => 'Baluchi',
				'bam' => 'Bambara',
				'ban' => 'Balinese',
				'baq' => 'Basque',
				'bas' => 'Basa (Cameroon)',
				'bat' => 'Baltic languages',
				'bej' => 'Beja',
				'bel' => 'Belarusian',
				'bem' => 'Bemba (Zambia)',
				'ben' => 'Bengali',
				'ber' => 'Berber languages',
				'bho' => 'Bhojpuri',
				'bih' => 'Bihari languages',
				'bik' => 'Bikol',
				'bin' => 'Bini',
				'bis' => 'Bislama',
				'bla' => 'Siksika',
				'bnt' => 'Bantu languages',
				'bod' => 'Tibetan',
				'bos' => 'Bosnian',
				'bra' => 'Braj',
				'bre' => 'Breton',
				'btk' => 'Batak languages',
				'bua' => 'Buriat',
				'bug' => 'Buginese',
				'bul' => 'Bulgarian',
				'bur' => 'Burmese',
				'byn' => 'Bilin',
				'cad' => 'Caddo',
				'cai' => 'Central American Indian languages',
				'car' => 'Galibi Carib',
				'cat' => 'Catalan',
				'cau' => 'Caucasian languages',
				'cba' => 'Chibchan languages',
				'ccn' => 'North Caucasian languages',
				'ccs' => 'South Caucasian languages',
				'cdc' => 'Chadic languages',
				'cdd' => 'Caddoan languages',
				'ceb' => 'Cebuano',
				'cel' => 'Celtic languages',
				'ces' => 'Czech',
				'cha' => 'Chamorro',
				'chb' => 'Chibcha',
				'che' => 'Chechen',
				'chg' => 'Chagatai',
				'chi' => 'Chinese',
				'chk' => 'Chuukese',
				'chm' => 'Mari (Russia)',
				'chn' => 'Chinook jargon',
				'cho' => 'Choctaw',
				'chp' => 'Chipewyan',
				'chr' => 'Cherokee',
				'chs' => 'Chinese (wrong code)',
				'chu' => 'Church Slavic',
				'chv' => 'Chuvash',
				'chy' => 'Cheyenne',
				'cmc' => 'Chamic languages',
				'cop' => 'Coptic',
				'cor' => 'Cornish',
				'cos' => 'Corsican',
				'cpe' => 'English based Creoles and pidgins',
				'cpf' => 'French-Based Creoles and pidgins',
				'cpp' => 'Portuguese-Based Creoles and pidgins',
				'cre' => 'Cree',
				'crh' => 'Crimean Tatar',
				'crp' => 'Creoles and pidgins',
				'csb' => 'Kashubian',
				'csu' => 'Central Sudanic languages',
				'cus' => 'Cushitic languages',
				'cym' => 'Welsh',
				'cze' => 'Czech',
				'dak' => 'Dakota',
				'dan' => 'Danish',
				'dar' => 'Dargwa',
				'day' => 'Land Dayak languages',
				'del' => 'Delaware',
				'den' => 'Slave (Athapascan)',
				'deu' => 'German',
				'dgr' => 'Dogrib',
				'din' => 'Dinka',
				'div' => 'Dhivehi',
				'dmn' => 'Mande languages',
				'doi' => 'Dogri (macrolanguage)',
				'dra' => 'Dravidian languages',
				'dsb' => 'Lower Sorbian',
				'dua' => 'Duala',
				'dum' => 'Middle Dutch (c. 1050–1350)',
				'dut' => 'Dutch',
				'dyu' => 'Dyula',
				'dzo' => 'Dzongkha',
				'efi' => 'Efik',
				'egx' => 'Egyptian languages',
				'egy' => 'Egyptian (Ancient)',
				'eka' => 'Ekajuk',
				'ell' => 'Modern Greek (1453-)',
				'elx' => 'Elamite',
				'eng' => 'English',
				'enm' => 'Middle English (1100-1500)',
				'epo' => 'Esperanto',
				'est' => 'Estonian',
				'esx' => 'Eskimo-Aleut languages',
				'euq' => 'Basque (family)',
				'eus' => 'Basque',
				'ewe' => 'Ewe',
				'ewo' => 'Ewondo',
				'fan' => 'Fang (Equatorial Guinea)',
				'fao' => 'Faroese',
				'fas' => 'Persian',
				'fat' => 'Fanti',
				'fij' => 'Fijian',
				'fil' => 'Filipino',
				'fin' => 'Finnish',
				'fiu' => 'Finno-Ugrian languages',
				'fon' => 'Fon',
				'fox' => 'Formosan languages',
				'fra' => 'French',
				'fre' => 'French',
				'frm' => 'Middle French (c. 1400–1600)',
				'fro' => 'Old French (842–c. 1400)',
				'frr' => 'Northern Frisian',
				'frs' => 'Eastern Frisian',
				'fry' => 'Western Frisian',
				'ful' => 'Fulah',
				'fur' => 'Friulian',
				'gaa' => 'Ga',
				'gay' => 'Gayo',
				'gba' => 'Gbaya (Central African Republic)',
				'gem' => 'Germanic languages',
				'geo' => 'Georgian',
				'ger' => 'German',
				'gez' => 'Ge\'ez',
				'gil' => 'Gilbertese',
				'gla' => 'Scottish Gaelic',
				'gle' => 'Irish',
				'glg' => 'Galician',
				'glv' => 'Manx',
				'gme' => 'East Germanic languages',
				'gmh' => 'Middle High German (c. 1050–1500)',
				'gmq' => 'North Germanic languages',
				'gmw' => 'West Germanic languages',
				'goh' => 'Old High German (c. 750–1050)',
				'gon' => 'Gondi',
				'gor' => 'Gorontalo',
				'got' => 'Gothic',
				'grb' => 'Grebo',
				'grc' => 'Ancient Greek (to 1453)',
				'gre' => 'Greek',
				'grk' => 'Greek languages',
				'grn' => 'Guarani',
				'gsw' => 'Swiss German',
				'guj' => 'Gujarati',
				'gwi' => 'Gwichʼin',
				'hai' => 'Haida',
				'hat' => 'Haitian',
				'hau' => 'Hausa',
				'haw' => 'Hawaiian',
				'heb' => 'Hebrew',
				'her' => 'Herero',
				'hil' => 'Hiligaynon',
				'him' => 'Himachali languages',
				'hin' => 'Hindi',
				'hit' => 'Hittite',
				'hmn' => 'Hmong',
				'hmo' => 'Hiri Motu',
				'hmx' => 'Hmong-Mien languages',
				'hok' => 'Hokan languages',
				'hrv' => 'Croatian',
				'hsb' => 'Upper Sorbian',
				'hun' => 'Hungarian',
				'hup' => 'Hupa',
				'hye' => 'Armenian',
				'hyx' => 'Armenian (family)',
				'iba' => 'Iban',
				'ibo' => 'Igbo',
				'ice' => 'Icelandic',
				'ido' => 'Ido',
				'iii' => 'Sichuan Yi',
				'iir' => 'Indo-Iranian languages',
				'ijo' => 'Ijo languages',
				'iku' => 'Inuktitut',
				'ile' => 'Interlingue',
				'ilo' => 'Iloko',
				'ina' => 'Interlingua (International Auxiliary Language Association)',
				'inc' => 'Indic languages',
				'ind' => 'Indonesian',
				'ine' => 'Indo-European languages',
				'inh' => 'Ingush',
				'ipk' => 'Inupiaq',
				'ira' => 'Iranian languages',
				'iro' => 'Iroquoian languages',
				'isl' => 'Icelandic',
				'ita' => 'Italian',
				'itc' => 'Italic languages',
				'jav' => 'Javanese',
				'jbo' => 'Lojban',
				'jpn' => 'Japanese',
				'jpr' => 'Judeo-Persian',
				'jpx' => 'Japanese (family)',
				'jrb' => 'Judeo-Arabic',
				'kaa' => 'Kara-Kalpak',
				'kab' => 'Kabyle',
				'kac' => 'Kachin',
				'kal' => 'Kalaallisut',
				'kam' => 'Kamba (Kenya)',
				'kan' => 'Kannada',
				'kar' => 'Karen languages',
				'kas' => 'Kashmiri',
				'kat' => 'Georgian',
				'kau' => 'Kanuri',
				'kaw' => 'Kawi',
				'kaz' => 'Kazakh',
				'kbd' => 'Kabardian',
				'kdo' => 'Kordofanian languages',
				'kha' => 'Khasi',
				'khi' => 'Khoisan languages',
				'khm' => 'Central Khmer',
				'kho' => 'Khotanese',
				'kik' => 'Kikuyu',
				'kin' => 'Kinyarwanda',
				'kir' => 'Kirghiz',
				'kmb' => 'Kimbundu',
				'kok' => 'Konkani (macrolanguage)',
				'kom' => 'Komi',
				'kon' => 'Kongo',
				'kor' => 'Korean',
				'kos' => 'Kosraean',
				'kpe' => 'Kpelle',
				'krc' => 'Karachay-Balkar',
				'krl' => 'Karelian',
				'kro' => 'Kru languages',
				'kru' => 'Kurukh',
				'kua' => 'Kuanyama',
				'kum' => 'Kumyk',
				'kur' => 'Kurdish',
				'kut' => 'Kutenai',
				'lad' => 'Ladino',
				'lah' => 'Lahnda',
				'lam' => 'Lamba',
				'lao' => 'Lao',
				'lat' => 'Latin',
				'lav' => 'Latvian',
				'lez' => 'Lezghian',
				'lim' => 'Limburgan',
				'lin' => 'Lingala',
				'lit' => 'Lithuanian',
				'lol' => 'Mongo',
				'loz' => 'Lozi',
				'ltz' => 'Luxembourgish',
				'lua' => 'Luba-Lulua',
				'lub' => 'Luba-Katanga',
				'lug' => 'Ganda',
				'lui' => 'Luiseno',
				'lun' => 'Lunda',
				'luo' => 'Luo (Kenya and Tanzania)',
				'lus' => 'Lushai',
				'mac' => 'Macedonian',
				'mad' => 'Madurese',
				'mag' => 'Magahi',
				'mah' => 'Marshallese',
				'mai' => 'Maithili',
				'mak' => 'Makasar',
				'mal' => 'Malayalam',
				'man' => 'Mandingo',
				'mao' => 'Maori',
				'map' => 'Austronesian languages',
				'mar' => 'Marathi',
				'mas' => 'Masai',
				'may' => 'Malay (macrolanguage)',
				'mdf' => 'Moksha',
				'mdr' => 'Mandar',
				'men' => 'Mende (Sierra Leone)',
				'mga' => 'Middle Irish (900-1200)',
				'mic' => 'Mi\'kmaq',
				'min' => 'Minangkabau',
				'mis' => 'Uncoded languages',
				'mkd' => 'Macedonian',
				'mkh' => 'Mon-Khmer languages',
				'mlg' => 'Malagasy',
				'mlt' => 'Maltese',
				'mnc' => 'Manchu',
				'mni' => 'Meitei',
				'mno' => 'Manobo languages',
				'moh' => 'Mohawk',
				'mon' => 'Mongolian',
				'mos' => 'Mossi',
				'mri' => 'Maori',
				'msa' => 'Malay (macrolanguage)',
				'mul' => 'Multiple languages',
				'mun' => 'Munda languages',
				'mus' => 'Creek',
				'mwl' => 'Mirandese',
				'mwr' => 'Marwari',
				'mya' => 'Burmese',
				'myn' => 'Mayan languages',
				'myv' => 'Erzya',
				'nah' => 'Nahuatl languages',
				'nai' => 'North American Indian',
				'nap' => 'Neapolitan',
				'nau' => 'Nauru',
				'nav' => 'Navajo',
				'nbl' => 'South Ndebele',
				'nde' => 'North Ndebele',
				'ndo' => 'Ndonga',
				'nds' => 'Low German',
				'nep' => 'Nepali (macrolanguage)',
				'new' => 'Nepal Bhasa',
				'ngf' => 'Trans-New Guinea languages',
				'nia' => 'Nias',
				'nic' => 'Niger-Kordofanian languages',
				'niu' => 'Niuean',
				'nld' => 'Dutch',
				'nno' => 'Norwegian Nynorsk',
				'nob' => 'Norwegian Bokmål',
				'nog' => 'Nogai',
				'non' => 'Old Norse',
				'nor' => 'Norwegian',
				'nqo' => 'N\'Ko',
				'nso' => 'Pedi',
				'nub' => 'Nubian languages',
				'nwc' => 'Classical Newari',
				'nya' => 'Nyanja',
				'nym' => 'Nyamwezi',
				'nyn' => 'Nyankole',
				'nyo' => 'Nyoro',
				'nzi' => 'Nzima',
				'oci' => 'Occitan (post 1500)',
				'oji' => 'Ojibwa',
				'omq' => 'Oto-Manguean languages',
				'omv' => 'Omotic languages',
				'ori' => 'Odiya (macrolanguage)',
				'orm' => 'Oromo',
				'osa' => 'Osage',
				'oss' => 'Ossetian',
				'ota' => 'Ottoman Turkish',
				'oto' => 'Otomian languages',
				'paa' => 'Papuan languages',
				'pag' => 'Pangasinan',
				'pal' => 'Pahlavi',
				'pam' => 'Pampanga',
				'pan' => 'Panjabi',
				'pap' => 'Papiamento',
				'pau' => 'Palauan',
				'peo' => 'Old Persian (c. 600–400 B.C.)',
				'per' => 'Persian',
				'phi' => 'Philippine languages',
				'phn' => 'Phoenician',
				'plf' => 'Central Malayo-Polynesian languages',
				'pli' => 'Pali',
				'pol' => 'Polish',
				'pon' => 'Pohnpeian',
				'por' => 'Portuguese',
				'poz' => 'Malayo-Polynesian languages',
				'pqe' => 'Eastern Malayo-Polynesian languages',
				'pqw' => 'Western Malayo-Polynesian languages',
				'pra' => 'Prakrit languages',
				'pro' => 'Old Provençal (to 1500)',
				'pus' => 'Pushto',
				'qaa-qtz' => 'Reserved for local use',
				'que' => 'Quechua',
				'qwe' => 'Quechuan (family)',
				'raj' => 'Rajasthani',
				'rap' => 'Rapanui',
				'rar' => 'Rarotongan',
				'rcf' => 'Reunionese, Reunion Creole',
				'roa' => 'Romance languages',
				'roh' => 'Romansh',
				'rom' => 'Romany',
				'ron' => 'Romanian',
				'rum' => 'Romanian',
				'run' => 'Rundi',
				'rup' => 'Macedo-Romanian',
				'rus' => 'Russian',
				'sad' => 'Sandawe',
				'sag' => 'Sango',
				'sah' => 'Yakut',
				'sai' => 'South American Indian languages',
				'sal' => 'Salishan languages',
				'sam' => 'Samaritan Aramaic',
				'san' => 'Sanskrit',
				'sas' => 'Sasak',
				'sat' => 'Santali',
				'scc' => 'Serbian',
				'scn' => 'Sicilian',
				'sco' => 'Scots',
				'scr' => 'Sicilian',
				'sdv' => 'Eastern Sudanic languages',
				'sel' => 'Selkup',
				'sem' => 'Semitic languages',
				'sga' => 'Old Irish (to 900)',
				'sgn' => 'Sign languages',
				'shn' => 'Shan',
				'sid' => 'Sidamo',
				'sin' => 'Sinhala',
				'sio' => 'Siouan languages',
				'sit' => 'Sino-Tibetan languages',
				'sla' => 'Slavic languages',
				'slk' => 'Slovak',
				'slo' => 'Slovak',
				'slv' => 'Slovenian',
				'sma' => 'Southern Sami',
				'sme' => 'Northern Sami',
				'smi' => 'Sami languages',
				'smj' => 'Lule Sami',
				'smn' => 'Inari Sami',
				'smo' => 'Samoan',
				'sms' => 'Skolt Sami',
				'sna' => 'Shona',
				'snd' => 'Sindhi',
				'snk' => 'Soninke',
				'sog' => 'Sogdian',
				'som' => 'Somali',
				'son' => 'Songhai languages',
				'sot' => 'Southern Sotho',
				'spa' => 'Spanish',
				'sqi' => 'Albanian',
				'srd' => 'Sardinian',
				'srn' => 'Sranan Tongo',
				'srp' => 'Serbian',
				'srr' => 'Serer',
				'ssa' => 'Nilo-Saharan languages',
				'ssw' => 'Swati',
				'suk' => 'Sukuma',
				'sun' => 'Sundanese',
				'sus' => 'Susu',
				'sux' => 'Sumerian',
				'swa' => 'Swahili (macrolanguage)',
				'swe' => 'Swedish',
				'syc' => 'Classical Syriac',
				'syd' => 'Samoyedic languages',
				'syr' => 'Syriac',
				'tah' => 'Tahitian',
				'tai' => 'Tai languages',
				'tam' => 'Tamil',
				'tat' => 'Tatar',
				'tbq' => 'Tibeto-Burman languages',
				'tel' => 'Telugu',
				'tem' => 'Timne',
				'ter' => 'Tereno',
				'tet' => 'Tetum',
				'tgk' => 'Tajik',
				'tgl' => 'Tagalog',
				'tha' => 'Thai',
				'tib' => 'Tibetan',
				'tig' => 'Tigre',
				'tir' => 'Tigrinya',
				'tiv' => 'Tiv',
				'tkl' => 'Tokelau',
				'tlh' => 'Klingon',
				'tli' => 'Tlingit',
				'tmh' => 'Tamashek',
				'tog' => 'Tonga (Nyasa)',
				'ton' => 'Tonga (Tonga Islands)',
				'tpi' => 'Tok Pisin',
				'trk' => 'Turkic languages',
				'tsi' => 'Tsimshian',
				'tsn' => 'Tswana',
				'tso' => 'Tsonga',
				'tuk' => 'Turkmen',
				'tum' => 'Tumbuka',
				'tup' => 'Tupi languages',
				'tur' => 'Turkish',
				'tut' => 'Altaic languages',
				'tuw' => 'Tungus languages',
				'tvl' => 'Tuvaluan',
				'twi' => 'Twi',
				'tyv' => 'Tuvinian',
				'udm' => 'Udmurt',
				'uga' => 'Ugaritic',
				'uig' => 'Uighur',
				'ukr' => 'Ukrainian',
				'umb' => 'Umbundu',
				'und' => 'Undetermined',
				'urd' => 'Urdu',
				'urj' => 'Uralic languages',
				'uzb' => 'Uzbek',
				'vai' => 'Vai',
				'ven' => 'Venda',
				'vie' => 'Vietnamese',
				'vol' => 'Volapük',
				'vot' => 'Votic',
				'wak' => 'Wakashan languages',
				'wal' => 'Wolaytta',
				'war' => 'Waray (Philippines)',
				'was' => 'Washo',
				'wel' => 'Welsh',
				'wen' => 'Sorbian languages',
				'wln' => 'Walloon',
				'wol' => 'Wolof',
				'xal' => 'Kalmyk',
				'xgn' => 'Mongolian languages',
				'xho' => 'Xhosa',
				'xnd' => 'Na-Dene languages',
				'yao' => 'Yao',
				'yap' => 'Yapese',
				'yid' => 'Yiddish',
				'yor' => 'Yoruba',
				'ypk' => 'Yupik languages',
				'zap' => 'Zapotec',
				'zbl' => 'Blissymbols',
				'zen' => 'Zenaga',
				'zgh' => 'Standard Moroccan Tamazight',
				'zha' => 'Zhuang',
				'zho' => 'Chinese',
				'zhx' => 'Chinese (family)',
				'zle' => 'East Slavic languages',
				'zls' => 'South Slavic languages',
				'zlw' => 'West Slavic languages',
				'znd' => 'Zande languages',
				'zul' => 'Zulu',
				'zun' => 'Zuni',
				'zxx' => 'No linguistic content',
				'zza' => 'Zaza'
			]
		],
	];

	foreach ($field_list as $field => $opts) {
		logStart('\'' . $field . '\'');
		$sql = ($opts['include_other'] ? $sqlWithOther : $sqlWithoutOther);
		$$field['query'] = $sql;

		foreach ($market_countries as $market => $countries) {
			logStart("\t$market",true);
			if ($opts['include_other']) {
				$$field['queries'][$market] = preg_replace(
					["/COUNTRY/","/FIELD/","/VALIDS/"],
					["AND `country` in ('" . implode("' ,'", $countries) . "')",$field,$opts['valids']],
					$$field['query']
				);
			} else {
				$$field['queries'][$market] = preg_replace(
					["/COUNTRY/","/FIELD/"],
					["AND `country` in ('" . implode("' ,'", $countries) . "')", $field],
					$$field['query']
				);
			}
			$res = $db->query($$field['queries'][$market]);
			$$field['results'][$market] = $res->fetchAll(PDO::FETCH_ASSOC);
			logEnd();
		}

		logStart("\tN/A",true);
		if ($opts['include_other']) {
			$$field['queries']['N/A'] = preg_replace(["/COUNTRY/","/FIELD/","/VALIDS/"], ["AND length(`country`) = 0", $field, $opts['valids']], $$field['query']);
		} else {
			$$field['queries']['N/A'] = preg_replace(["/COUNTRY/","/FIELD/"], ["AND length(`country`) = 0", $field], $$field['query']);
		}

		$res = $db->query($$field['queries']['N/A']);
		$$field['results']['N/A'] = $res->fetchAll(PDO::FETCH_ASSOC);
		logEnd();

		logStart("\tALL",true);
			$$field['queries'] = array();
		if ($opts['include_other']) {
			$$field['queries']['ALL'] = preg_replace(["/COUNTRY/","/FIELD/","/VALIDS/"], ["", $field, $opts['valids']], $$field['query']);
		} else {
			$$field['queries']['ALL'] = preg_replace(["/COUNTRY/","/FIELD/"], ["", $field], $$field['query']);
		}
		$res = $db->query($$field['queries']['ALL']);
		$$field['results']['ALL'] = $res->fetchAll(PDO::FETCH_ASSOC);
		logEnd();

		$data[$field] = $$field['results'];
	}

	logStart("Writing .dat",true);
	file_put_contents('results.dat', serialize($data));
	logEnd();
}


// End of Phase 2. Start Phase 3
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

$data = unserialize(file_get_contents('results.dat'));

$writer = fopen('output2.csv', 'w');
fputs( $writer, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

foreach ($data as $field => $market) {
	foreach ($market as $m_code => $index) {
		foreach ($index as $i => $row) {
			$value = $row[array_keys($row)[0]];
			$out[$field][$value][$m_code] = $row['#Filled'];
		}
	}
}

foreach ($out as $field => $field_val) {
	fputcsv($writer, [$field]);
	foreach ($field_val as $value => $m_code) {
		$line[0] = $value;
		for ($i = 1; $i <= count($market_countries); $i++) {
			$col = $i*4;
			$line[$col - 2] = '';
			$line[$col - 1] = '';
			$line[$col] = $out[$field][$value][array_keys($market_countries)[$i-1]] ?? 0;
			$line[$col + 1] = '';
		}
		fputcsv($writer, $line);
	}
}

fclose($writer);
